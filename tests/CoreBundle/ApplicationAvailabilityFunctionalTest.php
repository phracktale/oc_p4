<?php
// tests/CoreBundle/ApplicationAvailabilityFunctionalTest.php
namespace Tests\CoreBundle;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApplicationAvailabilityFunctionalTest extends WebTestCase
{
    /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);


        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function urlProvider()
    {
        return array(
            array('/'),
            array('/resa/billets'),
            array('/resa/visiteurs/CODE'),
            array('/resa/paymentSuccess/CODE}'),
            array('/resa/tickets/CODE'),
        );
    }
}