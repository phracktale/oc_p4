<?php
    namespace Tests\CoreBundle\Form\Type;

    use App\Phrack\CoreBundle\Form\Type\VisitorType;
    use App\Phrack\CoreBundle\Entity\Visitor;
    use DateTime;
    use Symfony\Component\Form\Test\TypeTestCase;

    class VisitorTypeTest extends TypeTestCase
    {
        public function testSubmitValidData()
        {
            $formData = [
                'firstname' => 'Thierry',
                'lastname' => 'Cazalet',
                'country' => 'France',
                'reductedPrice' => true,
            ];
            $form = $this->factory->create(VisitorType::class);

            $visitor = new Visitor();
            $visitor->fromArray($formData);


            // submit the data to the form directly
            $form->submit($formData);

            $this->assertTrue($form->isSynchronized());
            $this->assertEquals($visitor, $form->getData());

            $view = $form->createView();
            $children = $view->children;

            foreach (array_keys($formData) as $key) {
                    $this->assertArrayHasKey($key, $children);
            }
        }
    }