<?php
    // tests/AppBundle/Util/CalculatorTest.php
    namespace Tests\CoreBundle\Services;


    use Doctrine\ORM\EntityManager;
    use App\Phrack\CoreBundle\Services\Resa;
    use App\Phrack\CoreBundle\Entity\Visitor;
    use App\Phrack\CoreBundle\Entity\Product;
    use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

    class ResaTest extends KernelTestCase
    {
        /**
         * @var \Doctrine\ORM\EntityManager
         */
        private $em;

        private $container;

        protected function setUp()
        {
            self::bootKernel();

            //get the DI container
            $this->container = static::$kernel->getContainer();


            $this->em = $this->container->get('doctrine')->getManager();


        }

        public function testTarifAdulte()
        {
            $birthday = new \DateTime('1971-12-16');


            $visitor = new Visitor();
            $visitor->setBirthday($birthday); // adulte

            $product = $this->em->getRepository('PhrackCoreBundle:Product')->find(1); //billet journée

            $productVariation =  $this->container->get('phrack_core.resa')->getPrice($visitor, $product);

            // Le prix du ticket est égal à 16€
            $this->assertEquals(16, $productVariation->getAmount());
        }

        public function testTarifEnfant()
        {
            $birthday = new \DateTime('2010-12-16');


            $visitor = new Visitor();
            $visitor->setBirthday($birthday); // enfant

            $product = $this->em->getRepository('PhrackCoreBundle:Product')->find(1); //billet journée

            $productVariation =  $this->container->get('phrack_core.resa')->getPrice($visitor, $product);

            // Le prix du ticket est égal à 8€
            $this->assertEquals(8, $productVariation->getAmount());
        }
    }