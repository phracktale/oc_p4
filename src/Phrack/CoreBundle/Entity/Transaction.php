<?php

namespace App\Phrack\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Phrack\CoreBundle\Entity\Reservation;


/**
 * Transaction
 *
 * @ORM\Table(name="transaction")
 * @ORM\Entity(repositoryClass="App\Phrack\CoreBundle\Repository\TransactionRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Transaction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Phrack\CoreBundle\Entity\Reservation", inversedBy="transaction")
     * 
     */
    private $reservation;


    /**
     * @var string
     *
     * @ORM\Column(name="return_code", type="string", length=255)
     */
    private $returnCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_transaction", type="datetime")
     */
    private $dateTransaction;

    /**
     * @var string
     *
     * @ORM\Column(name="stripe_response", type="text")
     */
    private $stripeResponse;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set returnCode
     *
     * @param string $returnCode
     *
     * @return Transaction
     */
    public function setReturnCode($returnCode)
    {
        $this->returnCode = $returnCode;

        return $this;
    }

    /**
     * Get returnCode
     *
     * @return string
     */
    public function getReturnCode()
    {
        return $this->returnCode;
    }

    /**
     * Set dateTransaction
     *
     * @param \DateTime $dateTransaction
     *
     * @return Transaction
     */
    public function setDateTransaction($dateTransaction)
    {
        $this->dateTransaction = $dateTransaction;

        return $this;
    }

    /**
     * Get dateTransaction
     *
     * @return \DateTime
     */
    public function getDateTransaction()
    {
        return $this->dateTransaction;
    }

    /**
     * Set stripeResponse
     *
     * @param string $stripeResponse
     *
     * @return Transaction
     */
    public function setStripeResponse($stripeResponse)
    {
        $this->stripeResponse = $stripeResponse;

        return $this;
    }

    /**
     * Get stripeResponse
     *
     * @return string
     */
    public function getStripeResponse()
    {
        return $this->stripeResponse;
    }


    /**
     * Set reservation
     *
     * @param string $reservation
     *
     * @return Transaction
     */
    public function setReservation(Reservation $reservation)
    {
        $this->reservation = $reservation;

        return $this;
    }

    /**
     * Get reservation
     *
     * @return string
     */
    public function getReservation()
    {
        return $this->reservation;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->setDateTransaction = new \DateTime("now");
    }
}
