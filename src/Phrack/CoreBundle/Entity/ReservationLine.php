<?php

namespace App\Phrack\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Phrack\CoreBundle\Entity\ProductVariation;
use App\Phrack\CoreBundle\Entity\Reservation;

/**
 * reservation_line
 *
 * @ORM\Table(name="reservation_line")
 * @ORM\Entity(repositoryClass="App\Phrack\CoreBundle\Repository\ReservationLineRepository")
 */
class ReservationLine
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Phrack\CoreBundle\Entity\Reservation", inversedBy="reservationLine")
     * @ORM\JoinColumn(nullable=false)
     */
    private $reservation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Phrack\CoreBundle\Entity\ProductVariation")
     */
    private $productVariation;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255)
     */
    private $country;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="date")
     */
    private $birthdate;

    /**
     * @var decimal
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2)
     */
    private $price;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return reservation_line
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return reservation_line
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return reservation_line
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     *
     * @return reservation_line
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set productVariation
     *
     * @param ProductVariation $productVariation
     *
     * @return reservation_line
     */
    public function setProductVariation(ProductVariation $productVariation)
    {
        $this->productVariation = $productVariation;

        return $this;
    }

    /**
     * Get productVariation
     *
     * @return Product
     */
    public function getProductVariation()
    {
        return $this->productVariation;
    }


     /**
     * Set reservation
     *
     * @param Product $reservation
     *
     * @return reservation_line
     */
    public function setReservation(Reservation $reservation)
    {
        $this->reservation = $reservation;

        return $this;
    }

    /**
     * Get reservation
     *
     * @return Product
     */
    public function getReservation()
    {
        return $this->reservation;
    }


    /**
     * Set price
     *
     * @param decimal $price
     *
     * @return reservation_line
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return decimal
     */
    public function getPrice()
    {
        return $this->price;
    }
}
