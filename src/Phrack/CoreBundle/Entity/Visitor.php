<?php

namespace App\Phrack\CoreBundle\Entity;


class Visitor
{
    private $firstname;

    private $lastname;

    private $birthday;

    private $country;

    private $reductedPrice;

   

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

     public function getLastname()
    {
        return $this->lastname;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function getReductedPrice()
    {
        return $this->reductedPrice;
    }

    public function setReductedPrice($reductedPrice)
    {
        $this->reductedPrice = $reductedPrice;
    }

    public function addVisitors(Visitors $visitors)
    {
        if (!$this->visitors->contains($visitors)) {
            $this->visitors->add($visitors);
        }
    }


    public function fromArray($data = array())
    {
        foreach ($data as $property => $value) {

            $method = "set" . ucfirst($property);
            $this->$method($value);
        }
    }
}
