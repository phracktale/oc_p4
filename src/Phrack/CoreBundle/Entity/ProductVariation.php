<?php

namespace App\Phrack\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product_variation
 *
 * @ORM\Table(name="product_variation")
 * @ORM\Entity(repositoryClass="App\Phrack\CoreBundle\Repository\ProductVariationRepository")
 */
class ProductVariation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Phrack\CoreBundle\Entity\Product", inversedBy="productVariation")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="type_billet", type="string", length=255)
     */
    private $type_billet;


    /**
     * @var string
     *
     * @ORM\Column(name="tranche", type="string", length=255)
     */
    private $tranche;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type_billet
     *
     * @param string $type_billet
     *
     * @return Product_variation
     */
    public function setTypeBillet($type_billet)
    {
        $this->type_billet = $type_billet;

        return $this;
    }

    /**
     * Get type_billet
     *
     * @return string
     */
    public function getTypeBillet()
    {
        return $this->type_billet;
    }


    /**
     * Set tranche
     *
     * @param string $tranche
     *
     * @return Product_variation
     */
    public function setTranche($tranche)
    {
        $this->tranche = $tranche;

        return $this;
    }

    /**
     * Get tranche
     *
     * @return string
     */
    public function getTranchet()
    {
        return $this->tranche;
    }

     /**
     * Set description
     *
     * @param string $description
     *
     * @return Product_variation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return Product_variation
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }


    /**
     * Set product
     *
     * @param  Product $product
     *
     * @return Product_variation
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
