<?php

namespace App\Phrack\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Phrack\CoreBundle\Entity\Product;
use App\Phrack\CoreBundle\Entity\ReservationLine;
use App\Phrack\CoreBundle\Entity\Transaction;


/**
 * reservation
 *
 * @ORM\Table(name="reservation")
 * @ORM\Entity(repositoryClass="App\Phrack\CoreBundle\Repository\ReservationRepository")
 * @ORM\HasLifecycleCallbacks
 *
 */
class Reservation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Phrack\CoreBundle\Entity\ReservationLine", mappedBy="reservation")
     * @ORM\JoinColumn(nullable=false)
     */
    private $reservationLine;


    /**
     * @ORM\ManyToOne(targetEntity="App\Phrack\CoreBundle\Entity\Product")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\OneToMany(targetEntity="App\Phrack\CoreBundle\Entity\Transaction", mappedBy="reservation")
     * @ORM\JoinColumn(nullable=false)
     */
    private $transaction;

    /**
     * @var string
     *
     * @ORM\Column(name="code_reservation", type="string", length=255, unique=true)
     */
    private $codeReservation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_reservation", type="date")
     */
    private $dateReservation;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_tickets", type="integer")
     */
    private $nbTickets;

    /**
     * @var float
     *
     * @ORM\Column(name="total_amount", nullable=true, type="decimal", precision=10, scale=2))
     */
    private $totalAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="order_firstname", type="string", length=255, nullable=true)
     */
    private $orderFirstname;

    /**
     * @var string
     *
     * @ORM\Column(name="order_lastname", type="string", length=255, nullable=true)
     */
    private $orderLastname;

    /**
     * @var string
     *
     * @ORM\Column(name="order_email", type="string", length=255, nullable=true)
     */
    private $orderEmail;


    /**
     * @var string
     *
     * @ORM\Column(name="payment_status", type="string", length=255, nullable=true)
     */
    private $payment_status;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="date")
     */
    private $dateCreated;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeReservation
     *
     * @param string $codeReservation
     *
     * @return reservation
     */
    public function setCodeReservation($codeReservation)
    {
        $this->codeReservation = $codeReservation;

        return $this;
    }

    /**
     * Get codeReservation
     *
     * @return string
     */
    public function getCodeReservation()
    {
        return $this->codeReservation;
    }

    /**
     * Set dateReservation
     *
     * @param \DateTime $dateReservation
     *
     * @return reservation
     */
    public function setDateReservation($dateReservation)
    {
        $this->dateReservation = $dateReservation;

        return $this;
    }

    /**
     * Get dateReservation
     *
     * @return \DateTime
     */
    public function getDateReservation()
    {
        return $this->dateReservation;
    }

    /**
     * Set totalAmount
     *
     * @param float $totalAmount
     *
     * @return reservation
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * Get totalAmount
     *
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

     /**
     * Set nbTickets
     *
     * @param integer $nbTickets
     *
     * @return reservation
     */
    public function setNbTickets($nbTickets)
    {
        $this->nbTickets = $nbTickets;

        return $this;
    }

    /**
     * Get nbTickets
     *
     * @return integer
     */
    public function getNbTickets()
    {
        return $this->nbTickets;
    }

    /**
     * Set orderFirstname
     *
     * @param string $orderFirstname
     *
     * @return reservation
     */
    public function setOrderFirstname($orderFirstname)
    {
        $this->orderFirstname = $orderFirstname;

        return $this;
    }

    /**
     * Get orderFirstname
     *
     * @return string
     */
    public function getOrderFirstname()
    {
        return $this->orderFirstname;
    }

    /**
     * Set orderLastname
     *
     * @param string $orderLastname
     *
     * @return reservation
     */
    public function setOrderLastname($orderLastname)
    {
        $this->orderLastname = $orderLastname;

        return $this;
    }

    /**
     * Get orderLastname
     *
     * @return string
     */
    public function getOrderLastname()
    {
        return $this->orderLastname;
    }

    /**
     * Set orderEmail
     *
     * @param string $orderEmail
     *
     * @return reservation
     */
    public function setOrderEmail($orderEmail)
    {
        $this->orderEmail = $orderEmail;

        return $this;
    }

    /**
     * Get orderEmail
     *
     * @return string
     */
    public function getOrderEmail()
    {
        return $this->orderEmail;
    }


    /**
     * Set product
     *
     * @param string $produt
     *
     * @return reservation
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }



     /**
     * Set payment_status
     *
     * @param string $payment_status
     *
     * @return reservation
     */
    public function setPaymentStatus($payment_status)
    {
        $this->payment_status = $payment_status;

        return $this;
    }

    /**
     * Get payment_status
     *
     * @return string
     */
    public function getPaymentStatus()
    {
        return $this->payment_status;
    }


    /**
     * Set reservationLine
     *
     * @param string $produt
     *
     * @return reservationLine
     */
    public function setReservationLine(ReservationLine $reservationLine)
    {
        $this->reservationLine = $reservationLine;

        return $this;
    }

    /**
     * Get reservationLine
     *
     * @return string
     */
    public function getReservationLine()
    {
        return $this->reservationLine;
    }

    /**
     * Set transaction
     *
     * @param string $transaction
     *
     * @return reservationLine
     */
    public function setTransaction(Transaction $transaction)
    {
        $this->transaction = $transaction;

        return $this;
    }

    /**
     * Get transaction
     *
     * @return string
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    
     /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }


    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->dateCreated = new \DateTime("now");
    }
}
