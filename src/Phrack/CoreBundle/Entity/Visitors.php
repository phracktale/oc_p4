<?php
// src/AppBundle/Entity/Visitors.php
namespace App\Phrack\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Visitors
{
    protected $visitor;

    public function __construct()
    {
        $this->visitor = new ArrayCollection();
    }

    public function getVisitor()
    {
        return $this->visitor;
    }

    public function addVisitor(Visitor $visitor)
	{
	    $Visitor->addVisitors($this);

	    $this->visitor->add($visitor);
	}

}
