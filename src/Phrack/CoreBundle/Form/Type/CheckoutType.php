<?php

namespace App\Phrack\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class CheckoutType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
            $builder
                ->add("amount", HiddenType::class) 
                ->add("paymentToken", HiddenType::class) 
                ->add("currency", HiddenType::class) 
                ->add("firstname", TextType::class)
                ->add("lastname", TextType::class)
                ->add("email", TextType::class)
                ->add("numCard", TextType::class) 
                ->add("expiration", DateType::class, array('widget' => 'single_text', 'format' => 'dd/MM',)) 
                ->add("cvc", TextType::class)
                ->add('submit', SubmitType::class, [
                    'attr' => ['class' => 'btn btn-primary', 'value' => 'Payer']
                ]);
    }
}
