<?php

namespace App\Phrack\CoreBundle\Form\Type;

use App\Phrack\CoreBundle\Entity\Visitor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VisitorType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
            $builder
                ->add("firstname", TextType::class)
                ->add("lastname", TextType::class)
                ->add("birthday", DateType::class, ['widget' => 'single_text'])
                ->add("country", CountryType::class)
                ->add("reductedPrice", CheckboxType::class, ["required" => false]);
    } 

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Visitor::class,
        ));
    }

}
