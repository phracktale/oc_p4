<?php

namespace App\Phrack\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class ResaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateReservation', HiddenType::class)
            ->add('product', EntityType::class, array(
                'class' => 'PhrackCoreBundle:Product',
                'choice_label' => 'title',
                'expanded' => false,
                'multiple' => false
            ))
            ->add('nb_tickets', IntegerType::class, array('attr' => array('min' => 1, 'max' => 10)))
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary', 'value' => 'Suivant']
            ]);
    } 

    
}
