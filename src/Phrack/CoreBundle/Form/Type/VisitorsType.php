<?php

namespace App\Phrack\CoreBundle\Form\Type;

use App\Phrack\CoreBundle\Entity\Visitors;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VisitorsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('visitor', CollectionType::class, array(
                'entry_type' => VisitorType::class,
                'allow_add'    => true,
            ))
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary', 'value' => 'Suivant']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Visitors::class,
        ));
    }
}
