<?php

namespace App\Phrack\CoreBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;


class PhrackCoreExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
       $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $container->register('matrixCode', 'BG\BarcodeBundle\Util\Base2DBarcode');
        $container->register('fpdi', 'FPDI');
    }
}
