<?php

namespace App\Phrack\CoreBundle\Services;

use App\Phrack\CoreBundle\Entity\Reservation;
use App\Phrack\CoreBundle\Entity\Product;
use App\Phrack\CoreBundle\Entity\ReservationLine;
use App\Phrack\CoreBundle\Entity\Transaction;
use App\Phrack\CoreBundle\Entity\Visitor;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class Resa extends Controller
{
    protected $em;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function existsReservationCode($code)
    {
        $reservation = $this->em->getRepository('PhrackCoreBundle:Reservation')->findOneBy(["codeReservation" => $code]);

        if ($reservation instanceof Reservation) {
            return true;
        }
        return  false;
    }

    public function newResa($dateReservation, $product, $nb_tickets)
    {
        $product = $this->em->getRepository('PhrackCoreBundle:Product')->find($product->getId());
        $reservation = new Reservation();
        $reservation->setCodeReservation($this->newResaCode());
        $reservation->setDateReservation(new \DateTime($dateReservation));
        $reservation->setNbTickets($nb_tickets);
        $reservation->setProduct($product);
        $this->em->persist($reservation);
        $this->em->flush();
        
        return  ['reservation' => $reservation];
    }

    public function newResaCode($len=10)
    {
        $hex = md5("l0Uvr3" . uniqid("", true));
        $pack = pack('H*', $hex);
        $tmp =  base64_encode($pack);
        $uid = preg_replace("#(*UTF8)[^A-Z0-9]#", "", $tmp);
        $len = max(4, min(128, $len));

        $uid = substr($uid, 0, $len);
        return $uid;
    }

    public function getVisitors($fields, $code)
    {
        $reservation = $this->em->getRepository('PhrackCoreBundle:Reservation')->findOneBy(["codeReservation" => $code]);

        $visitors = $fields->getVisitor();

        $total_amount = 0;

        foreach($visitors as $visitor)
        {
           $firstname = $visitor->getFirstname();
           $lastname = $visitor->getLastname();
           $birthday = $visitor->getBirthday();
           $country = $visitor->getCountry();
           $productVariation = $this->getPrice($visitor, $reservation->getProduct());
           $price = $productVariation->getAmount();
           $total_amount += $price;
           $reservationLine = new ReservationLine();
           $reservationLine->setProductVariation($productVariation);
           $reservationLine->setReservation($reservation);
           $reservationLine->setFirstname($firstname);
           $reservationLine->setLastname($lastname);
           $reservationLine->setBirthdate($birthday);
           $reservationLine->setCountry($country);
           $reservationLine->setPrice($price);
           $this->em->persist($reservationLine);
        }

        $reservation->setTotalAmount($total_amount);

        $this->em->persist($reservation);

        $this->em->flush();

        return true;
    }

    /**
     * @param $fields
     * @param $code
     * @param $stripeClient
     * @return \stdClass
     */
    public function updateReservationPayment($fields, $code, $stripeClient)
    {
        $reservation = $this->em->getRepository('PhrackCoreBundle:Reservation')->findOneBy(["codeReservation" => $code]);

        $firstname = $fields['firstname'];
        $lastname = $fields['lastname'];
        $email = $fields['email'];
        
        $paymentToken = $fields['paymentToken'];
        $chargeAmount = $fields['amount']*100;


        try
        {
            $charge = $stripeClient->createCharge($chargeAmount, "EUR", $paymentToken);
        }
        catch (\Stripe\Error\Card $e)
        {
          $charge = new  \stdClass();
          $charge->paid= false;
          $charge->reason = $e->getMessage();
          return $charge;

        }

        $reservation->setOrderFirstname($firstname);
        $reservation->setOrderLastname($lastname);
        $reservation->setOrderEmail($email);
        $reservation->setPaymentStatus($charge->status);
        $this->em->persist($reservation);

        
        $transaction = new Transaction();
        $transaction->setReturnCode($charge->status);

        $datc = new \DateTime();
        $datc->setTimestamp($charge->created);
        $transaction->setDateTransaction($datc);
        $transaction->setStripeResponse($charge);
        $transaction->setReservation($reservation);
        $this->em->persist($transaction);

        $this->em->flush();
        return $charge;
    }

    

    public function getPrice(Visitor $visitor, Product $product)
    {
        $birthday = $visitor->getBirthday();
        $age = $this->getAge($birthday->format('Y-m-d'));

        if($visitor->getReductedPrice())
        {
            $tranche = "";
            $type_billet = "reduit";
        }
        elseif($age < 4)
        {
            $tranche = "bébé";
            $type_billet = "gratuit";
        }
        else if($age > 4 && $age < 12)
        {
            $tranche = "enfant";
            $type_billet = "normal";
        }
        else if($age > 12 && $age < 60)
        {
            $tranche = "adulte";
            $type_billet = "normal";
        }
        else if($age >  60)
        {
            $tranche = "senior";
            $type_billet = "normal";
        }

        $productVariation = $this->em->getRepository('PhrackCoreBundle:ProductVariation')->findOneBy(["type_billet" => $type_billet, "tranche" => $tranche, "product" => $product]);

        if($productVariation)
        {
            return $productVariation;
        }

        return false;
    }

    public function getAge($birthdate)
    {
        $from = new \DateTime($birthdate);
        $to   = new \DateTime('today');
        return $from->diff($to)->y;
    }

    public function isTypeTicketValid($dateReservation, $type_billet)
    {
        
        $today = new \DateTime('NOW');

        /* Test après 14h */
        if( ($dateReservation == $today->format('Y-m-d') ) && ( $type_billet == 'jour' ) && ($today->format('H') > 14))
        {
            return ["result" => false, "level" => "warning" , "message" => "Le billet journée n'est plus disponible après 14h."];
        }

        /* Test nb billets vendus dans la journée */


        return ["result" => true, "level" => "warning", "message" => ""];
    }
}
