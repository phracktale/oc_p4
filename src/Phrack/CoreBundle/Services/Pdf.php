<?php
namespace App\Phrack\CoreBundle\Services;

use App\Phrack\CoreBundle\Entity\Reservation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BG\BarcodeBundle\Util\Base2DBarcode as matrixCode;
use \FPDI;

class Pdf extends Controller
{
    protected $templateTicket;

    protected $container;

    protected $fpdi;

    protected $matrixCode;

    public function __construct($container, FPDI $fpdi, matrixCode $matrixCode)
    {
       $this->container = $container;
       $this->fpdi = $fpdi;
       $this->matrixCode = $matrixCode;
       $this->templateTicket = '@PhrackCoreBundle/Resources/pdf/billet.pdf';
        
    }

    public function getTicket(Reservation $reservation)
    {
        $myBarcode = $this->matrixCode;

        $myBarcode->savePath = '/tmp/';
        $bcPathAbs = $myBarcode->getBarcodePNGPath($reservation->getCodeReservation(), 'DATAMATRIX', 150, 150);


        $pdf = $this->fpdi;

        $kernel = $this->container->get('kernel');
        $billet_pdf_path = $kernel->locateResource($this->templateTicket);

        $pdf->setSourceFile($billet_pdf_path);

        foreach($reservation->getReservationLine() as $resaLine) {
            $pdf->AddPage(); // add a page
            $tplIdx = $pdf->importPage(1);
            // use the imported page and place it at point 10,10 with a width of 100 mm

            $pdf->useTemplate($tplIdx, 0, 0, 210);
            /* Envoi d'um email avec les billets */

            $pdf->SetFont('Arial', '', 12);
            $pdf->SetTextColor(0, 0, 0);
            $porteur = $resaLine->getFirstname() . " " . $resaLine->getLastname();
            $pdf->Text(145, 28, utf8_decode($resaLine->getProductVariation()->getProduct()->getTitle()), '', '', false);
            $pdf->Text(155, 33, $reservation->getDateReservation()->format('d/m/Y'), '', '', false);
            $pdf->Text(135, 64, $resaLine->getPrice() . iconv('UTF-8', 'windows-1252', "€"), '', '', false);

            $pdf->Text(22, 68, utf8_decode($porteur), '', '', false);
            $pdf->Text(22, 75, utf8_decode($resaLine->getProductVariation()->getProduct()->getDescription()), '', '', false);
            $pdf->SetXY(20, 80);
            $pdf->MultiCell(150, 6, iconv('UTF-8', 'windows-1252', $resaLine->getProductVariation()->getDescription()), 0, 1, false);

            $pdf->Text(60, 117, $reservation->getDateCreated()->format('d/m/Y'), '', '', false);
            $pdf->Text(60, 128, utf8_decode($reservation->getCodeReservation()), '', '', false);
            $pdf->Image($bcPathAbs, 180, 80, 15, 15, 'PNG');
            $pdf->SetFont('Arial', '', 7);
            $pdf->Text(180, 98, utf8_decode($reservation->getCodeReservation()), '', '', false);
        }

        return $pdf;
    }

 }
