<?php

namespace App\Phrack\CoreBundle\Services;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class Emailer extends Controller
{
   protected $mailer;
    
    
    public function __construct($mailer)
    {
        $this->mailer = $mailer;
        
    }

    public function send($subject, $message, $from, $to, $attach)
    {
        $attachment = new \Swift_Attachment($attach, 'billets.pdf', 'application/pdf');

        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom($from)
            ->setTo($to)
            ->setBody($message, 'text/html')
            ->attach($attachment);

        if (!$this->mailer->send($message))
        {
            return ["result" => false, "level" => "warning" , "message" => "Il y a eu un problème à l'envoi de l'email, n'oubliez pas de téléchargeer vos billets ci-dessous."];
        }
        else
        {
           return ["result" => true, "level" => "success" , "message" => "Un email de confirmation vous est envoyé avec vos tickets"];
        }

    }
}
