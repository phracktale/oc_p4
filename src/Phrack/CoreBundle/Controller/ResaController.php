<?php

namespace App\Phrack\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Phrack\CoreBundle\Form\Type\ResaType;
use App\Phrack\CoreBundle\Form\Type\VisitorsType;
use App\Phrack\CoreBundle\Form\Type\CheckoutType;
use App\Phrack\CoreBundle\Entity\Visitors;
use App\Phrack\CoreBundle\Entity\Visitor;
use App\Phrack\CoreBundle\Entity\Transaction;

class ResaController extends Controller
{
    public function indexAction()
    {
        $this->get('session')->clear();

        return $this->render('PhrackCoreBundle:Resa:home.html.twig');
    }


    public function formResaDateAction(Request $request)
    {
        $testTicket = null;
        $today = date('Y-m-d');
        $products = $this->getDoctrine()->getManager()->getRepository('PhrackCoreBundle:Product')->findAll();
        $form = $this->get('form.factory')->create(ResaType::class, ['products' => $products]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $fields = $form->getData();
            $testTicket = $this->get('phrack_core.resa')->isTypeTicketValid($fields['dateReservation'], $fields['product']->getCode());
            if ($testTicket['result']) {
                $result = $this->get('phrack_core.resa')->newResa($fields['dateReservation'], $fields['product'], $fields['nb_tickets']);
                if ($result['reservation']) {
                    return $this->redirectToRoute('Phrack_core_formResaVisiteurs', array('code' => $result['reservation']->getCodeReservation()));
                }
            }
        }
        return $this->render('PhrackCoreBundle:Resa:order_step1_formResa.html.twig', array(
            'form' => $form->createView(),
            'today' => $today,
            'flash' => $testTicket
        ));
    }

    /**
     * @param Request $request
     * @param $code
     * @return mixed
     */
    public function formResaVisiteursAction(Request $request, $code)
    {
        if(!$code || !$this->get('phrack_core.resa')->existsReservationCode($code)){return $this->render('PhrackCoreBundle:Resa:reservationError.html.twig');}
        $reservation = $this->getDoctrine()->getManager()->getRepository('PhrackCoreBundle:Reservation')->FindOneBy(array('codeReservation' => $code,));
        $visitors = new Visitors();
        $nbTickets = $reservation->getNbTickets();
        for ($k = 0; $k < $nbTickets; $k++) {
            $visitor = new Visitor();
            $visitors->getVisitor()->add($visitor);
        }
        $form = $this->get('form.factory')->create(VisitorsType::class, $visitors);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
           $result = $this->get('phrack_core.resa')->getVisitors($form->getData(), $code);
            if ($result) {
                return $this->redirectToRoute('Phrack_core_formResaCheckout', array('code' => $code));
            }
            return $this->render('PhrackCoreBundle:Resa:order_step2_visiteurs.html.twig',array('form' => $form->createView()));
        }
        return $this->render('PhrackCoreBundle:Resa:order_step2_visiteurs.html.twig', array('form' => $form->createView()));
    }


    public function formResaCheckoutAction(Request $request, $code)
    {
        if(!$code || !$this->get('phrack_core.resa')->existsReservationCode($code)){  return $this->render('PhrackCoreBundle:Resa:reservationError.html.twig'); }
        $charge = null;
        $flash = null;
        $reason = "";
        $reservation = $this->getDoctrine()->getManager()->getRepository('PhrackCoreBundle:Reservation')->findOneBy(array('codeReservation' => $code,));
        $visitors = $reservation->getReservationLine();
        $form = $this->get('form.factory')->create(CheckoutType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $stripeClient = $this->get('flosch.stripe.client');
            $charge = $this->get('phrack_core.resa')->updateReservationPayment($form->getData(), $code, $stripeClient);

            if ($charge->paid) {
                return $this->redirectToRoute('Phrack_core_formResaPaymentSuccess', array('code' => $code ));
            }
            $flash = ["level" => "warning", "message" => "Le paiement n'a pas pu aboutir:" . $charge->reason];
        }

        return $this->render(
            'PhrackCoreBundle:Resa:order_step3_checkout.html.twig',
            array(
                'form' => $form->createView(),
                'reservation' => $reservation,
                'visitors' => $visitors,
                'stripe_public_key' => 'pk_test_CFAiNla8u2atPa6NhVMB2GcH', #$this->getParameter('stripe_public_key'),
                'amount' => $reservation->getTotalAmount(),
                'flash' => $flash
            )
        );

    }

    public function formResaPaymentSuccessAction(Request $request, $code)
    {
        if(!$code || !$this->get('phrack_core.resa')->existsReservationCode($code)){  return $this->render('PhrackCoreBundle:Resa:reservationError.html.twig'); }
        $reservation = $this->getDoctrine()->getManager()->getRepository('PhrackCoreBundle:Reservation')->FindOneBy(array('codeReservation' => $code,));
        $pdf = $this->get('phrack_core.pdf')->getTicket($reservation);

        $vars = [
            'nom' => $reservation->getOrderLastname(),
            'prenom' => $reservation->getOrderFirstname(),
            'code_reservation' => $reservation->getCodeReservation(),
            'date_visite' => $reservation->getDateReservation()->format('d/m/Y'),
            'visitors' => $reservation->getReservationLine(),
            'type_billet' => $reservation->getProduct()->getTitle(),
            'billet_description' => $reservation->getProduct()->getDescription(),
            'total' => $reservation->getTotalAmount()
        ];

        $message = $this->renderView( 'PhrackCoreBundle:Emails:confirmation.html.twig', array( 'var' => $vars )  );

        $subject = 'Vos tickets pour le musée du Louvre';
        $from = "no-reply@phracktale.com";
        $to = $reservation->getOrderEmail();
        $mailer = $this->get('phrack_core.emailer')->send($subject, $message, $from, $to, $pdf->Output('S'));

        return $this->render( 'PhrackCoreBundle:Resa:order_step4_paymentsuccess.html.twig',
            array(
                'code_reservation' => $code,
                'reservation' => $reservation,
                'flash' => $mailer
            )
        );
    }

    public function formResaDownloadTicketAction(Request $request, $code)
    {
        if(!$code || !$this->get('phrack_core.resa')->existsReservationCode($code))  { return $this->render('PhrackCoreBundle:Resa:reservationError.html.twig');  }

        $reservation = $this->getDoctrine()->getManager()->getRepository('PhrackCoreBundle:Reservation')->FindOneBy(array('codeReservation' => $code,));
        $pdf = $this->get('phrack_core.pdf')->getTicket($reservation);

        new Response($pdf->Output(), 200, array('Content-Type' => 'application/pdf'));

        return $this->render(
            'PhrackCoreBundle:Resa:order_step4_paymentsuccess.html.twig',
            array(
                'code_reservation' => $code,
                'reservation' => $reservation
            )
        );
    }
}
